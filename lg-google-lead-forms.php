<?php
/*
Plugin Name: Longevity Google Lead Forms
Plugin URI: https://www.longevitygraphics.com/
description: A plugin for webhook integration with Google lead forms
Version: 1.0.0
Author: Longevity Graphics, Gurpreet Dhanju
Author URI: https://www.longevitygraphics.com/
*/
define( 'LG_GOOGLE_LEAD_FORMS_PLUGIN', __FILE__ );
define(
	'LG_GOOGLE_LEAD_FORMS_PLUGIN_DIR',
	untrailingslashit( dirname( LG_GOOGLE_LEAD_FORMS_PLUGIN ) )
);

require_once LG_GOOGLE_LEAD_FORMS_PLUGIN_DIR . '/includes/grid.php';
require_once LG_GOOGLE_LEAD_FORMS_PLUGIN_DIR . '/includes/route.php';
require_once LG_GOOGLE_LEAD_FORMS_PLUGIN_DIR . '/includes/settings.php';



add_action(
	'activated_plugin',
	function ( $plugin ) {
		if ( plugin_basename( __FILE__ ) === $plugin ) {
			wp_safe_redirect( admin_url( 'edit.php?post_type=lg-google-lead&page=lg_google_lead_forms' ) );
			exit;
		}
	}
);

