<?php

/**
 * Custom option and settings.
 */
function lg_google_lead_forms_settings_init() {
	// Register a new setting for "lg_google_lead_forms" page.
	register_setting( 'lg_google_lead_forms', 'lg_google_lead_forms_options' );
	register_setting( 'lg_google_lead_forms', 'lg_option_google_analytics_id' );

	// Register a new section in the "lg_google_lead_forms" page.
	add_settings_section(
		'lg_google_lead_forms_section_content',
		__( 'Google lead forms settings', 'lg_google_lead_forms' ),
		'lg_google_lead_forms_section_content_cb',
		'lg_google_lead_forms'
	);
	add_settings_field(
		'lg_google_lead_forms_google_key',
		__( 'Google Key', 'lg_google_lead_forms' ),
		'lg_google_lead_forms_google_key_cb',
		'lg_google_lead_forms',
		'lg_google_lead_forms_section_content',
		array(
			'label_for'                        => 'lg_google_lead_forms_google_key',
			'class'                            => 'lg_google_lead_forms_row_google_key',
			'lg_google_lead_forms_custom_data' => 'custom',
		)
	);
	add_settings_field(
		'lg_google_lead_forms_email',
		__( 'Receiver Email', 'lg_google_lead_forms' ),
		'lg_google_lead_forms_email_cb',
		'lg_google_lead_forms',
		'lg_google_lead_forms_section_content',
		array(
			'label_for'                        => 'lg_google_lead_forms_email',
			'class'                            => 'lg_google_lead_forms_row_email',
			'lg_google_lead_forms_custom_data' => 'custom',
		)
	);

	add_settings_field(
		'lg_option_google_analytics_id',
		__( 'Google Analytics Id', 'lg_google_lead_forms' ),
		'lg_option_google_analytics_id_cb',
		'lg_google_lead_forms',
		'lg_google_lead_forms_section_content',
		array(
			'label_for'                        => 'lg_option_google_analytics_id',
			'class'                            => 'lg_option_google_analytics_id_row',
			'lg_google_lead_forms_custom_data' => 'custom',
		)
	);

}

/**
 * Register our lg_google_lead_forms_settings_init to the admin_init action hook.
 */
add_action( 'admin_init', 'lg_google_lead_forms_settings_init' );


/**
 * Section callbacks can accept an $args parameter, which is an array.
 * the values are defined at the add_settings_section() function.
 *
 * @param array $args $args have the following keys defined: title, id, callback.
 */
function lg_google_lead_forms_section_content_cb( $args ) {
	?>
	<br>
	<p>Webhook URI</p>
	<code><?php echo esc_url( get_site_url() . '/wp-json/lg-google-lead-forms/v1/save' ); ?></code>
	<?php
}

/**
 * Custom option "Receiver Email" callback functions.
 *
 * @param array $args arguments have information about the input field.
 */
function lg_google_lead_forms_email_cb( $args ) {
	// Get the value of the setting we've registered with register_setting().
	$options = get_option( 'lg_google_lead_forms_options' );
	$content = isset( $options['lg_google_lead_forms_email'] ) ? $options['lg_google_lead_forms_email'] : false;
	?>
	<input
		type="text"
		name="lg_google_lead_forms_options[lg_google_lead_forms_email]"
		value="<?php echo wp_kses_post( $content ); ?>"
		style="width: 400px"/>
	<?php
}

/**
 * Custom option "Receiver Email" callback functions.
 *
 * @param array $args arguments have information about the input field.
 */
function lg_option_google_analytics_id_cb( $args ) {
	// Get the value of the setting we've registered with register_setting().
	$lg_option_google_analytics_id = get_option( 'lg_option_google_analytics_id' );

	?>
	<input
		type="text"
		name="lg_option_google_analytics_id"
		value="<?php echo wp_kses_post( $lg_option_google_analytics_id ); ?>"
		style="width: 400px"/>
	<p class="description">This field is same as the one on <a href="<?php echo esc_html( get_site_url() ); ?>/wp-admin/admin.php?page=longevity_site_config#analytics">Longevity Framework > Theme Setup > Site Options > Analytics Tracking > Google Analytics</a></p>
	<?php
}


/**
 * Custom option "Google Key" callback functions.
 *
 * @param array $args arguments have information about the input field.
 */
function lg_google_lead_forms_google_key_cb( $args ) {
	// Get the value of the setting we've registered with register_setting().
	$options = get_option( 'lg_google_lead_forms_options' );
	$content = isset( $options['lg_google_lead_forms_google_key'] ) ? $options['lg_google_lead_forms_google_key'] : false;
	?>
	<input
		type="text"
		name="lg_google_lead_forms_options[lg_google_lead_forms_google_key]"
		value="<?php echo wp_kses_post( $content ); ?>"
		style="width: 400px"/>
	<?php
}


/**
 * This is necessary for multisite.
 */
function lg_google_lead_forms_plugins_loaded() {
	/**
	 * Register our lg_google_lead_forms_options_page to the admin_menu action hook
	 */
	add_action( 'admin_menu', 'lg_google_lead_forms_options_page' );
}

add_action( 'plugins_loaded', 'lg_google_lead_forms_plugins_loaded' );

/**
 * Settings sub menu
 */
function lg_google_lead_forms_options_page() {
	// Add Settings sub menu.
	add_submenu_page(
		'edit.php?post_type=lg-google-lead',
		'Settings',
		'Settings',
		'manage_options',
		'lg_google_lead_forms',
		'lg_google_lead_forms_options_page_html'
	);
}

/**
 * Settings sub menu.
 * callback functions
 */
function lg_google_lead_forms_options_page_html() {
	// Check user capabilities.
	if ( ! current_user_can( 'manage_options' ) ) {
		return;
	}

	// Add error/update messages.

	// Check if the user have submitted the settings.
	// WordPress will add the "settings-updated" $_GET parameter to the url.

	if ( isset( $_GET['settings-updated'] ) ) {

		// Add settings saved message with the class of "updated".
		add_settings_error( 'lg_google_lead_forms_messages', 'lg_google_lead_forms_message', __( 'Settings Saved', 'lg_google_lead_forms' ), 'updated' );
	}

	// Show error/update messages.
	settings_errors( 'lg_google_lead_forms_messages' );
	?>
	<div class="wrap">
		<h1><?php echo esc_html( get_admin_page_title() ); ?></h1>
		<form action="options.php" method="post">
			<?php
			// Output security fields for the registered setting "lg_google_lead_forms".
			settings_fields( 'lg_google_lead_forms' );
			// Output setting sections and their fields.
			// (sections are registered for "lg_google_lead_forms", each field is registered to a specific section).
			do_settings_sections( 'lg_google_lead_forms' );
			// Output save settings button.
			submit_button( 'Save Settings' );
			?>
		</form>
	</div>
	<?php
}
