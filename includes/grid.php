<?php

add_action(
	'init',
	function () {
		register_post_type(
			'lg-google-lead',
			array(
				'labels'             => array(
					'name'          => __( 'Google Leads', 'lg' ),
					'singular_name' => __( 'Google Lead', 'lg' ),
					'edit_item'     => __( 'View Lead', 'lg' ),
				),
				'supports'           => array( '' ),
				'public'             => true,
				'publicly_queryable' => false,
				'show_ui'            => true,
				'show_in_menu'       => true,
				'has_archive'        => false,
				'show_in_rest'       => false,
				'map_meta_cap'       => true,
				'capabilities'       => array(
					'create_posts' => false,
				),
			)
		);
	}
);

// Add columns to grid.
add_filter(
	'manage_lg-google-lead_posts_columns',
	function ( $columns ) {
		$columns['email'] = __( 'Email', 'lg' );

		return array(
			'cb'             => $columns['cb'],
			'title'          => $columns['title'],
			'EMAIL'          => __( 'Email', 'lg' ),
			'PHONE_NUMBER'   => __( 'Phone', 'lg' ),
			'STREET_ADDRESS' => __( 'Street Address', 'lg' ),
			'CITY'           => __( 'City', 'lg' ),
			'REGION'         => __( 'Region', 'lg' ),
			'COUNTRY'        => __( 'Country', 'lg' ),
			'POSTAL_CODE'    => __( 'Postal Code', 'lg' ),
			'COMPANY_NAME'   => __( 'Company Name', 'lg' ),
			'date'           => __( 'Date', 'lg' ),
		);
	}
);

// Populate columns.
add_action(
	'manage_lg-google-lead_posts_custom_column',
	function ( $column, $post_id ) {
		echo esc_html( get_post_meta( $post_id, $column, true ) );
	},
	10,
	2
);

add_action( 'add_meta_boxes', 'extra_info_add_meta_boxes' );
if ( ! function_exists( 'extra_info_add_meta_boxes' ) ) {
	function extra_info_add_meta_boxes() {
		global $post;

		add_meta_box( 'extra_info_data', __( 'Lead Information', 'lg' ), 'extra_info_data_content', 'lg-google-lead', 'normal', 'core' );
	}
}

function extra_info_data_content() {
	global $post;
	$lead_fields = get_post_meta( get_the_ID() );
	$lead_fields = array_combine( array_keys( $lead_fields ), array_column( $lead_fields, '0' ) );
	?>
	<h1><?php echo wp_kses_post( get_the_title() ); ?></h1>

	<p><b><?php echo get_the_date(); ?></b></p>
	<ul>
		<?php
		foreach ( $lead_fields as $key => $value ) :
			if ( '_edit_lock' === $key ) {
				continue;
			}
			?>
			<li><?php echo '<b>' . wp_kses_post( $key ) . '</b>: ' . wp_kses_post( $value ); ?></li>
		<?php endforeach; ?>
	</ul>

	<br>
	<p><b>Lead Id: </b><i><?php echo wp_kses_post( get_the_content() ); ?></i></p>
	<?php
}

add_filter(
	'post_row_actions',
	function ( $actions ) {
		if ( get_post_type() === 'lg-google-lead' ) {
			unset( $actions['edit'] );
			unset( $actions['view'] );
			unset( $actions['inline hide-if-no-js'] );
		}

		return $actions;
	},
	10,
	1
);

// Remove the submitdiv metabox.
add_action(
	'admin_menu',
	function () {
		remove_meta_box( 'submitdiv', 'lg-google-lead', 'side' );
	}
);


// Remove the Edit bulk action.
add_filter(
	'bulk_actions-edit-lg-google-lead',
	function ( $actions ) {
		unset( $actions['edit'] );
		return $actions;
	}
);

