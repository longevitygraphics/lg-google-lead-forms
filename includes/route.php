<?php
add_action(
	'rest_api_init',
	function () {
		register_rest_route(
			'lg-google-lead-forms/v1',
			'save',
			array(
				'methods'             => 'POST',
				'callback'            => 'lg_google_lead_forms_save',
				'permission_callback' => '__return_true',
			)
		);
	}
);

/**
 * Callback function for lg-google-lead-forms/v1/save route
 */
function lg_google_lead_forms_save( $request ) {

	$request_body_array = array();
	$request_body       = $request->get_body();
	if ( isset( $request_body ) && ! empty( $request_body ) ) {
		$request_body_array = json_decode( $request_body, true );
	}

	// Get the google key from options.
	$lg_google_lead_options = get_option( 'lg_google_lead_forms_options' );
	$google_key             = ( isset( $lg_google_lead_options['lg_google_lead_forms_google_key'] ) ) ? $lg_google_lead_options['lg_google_lead_forms_google_key'] : '';
	$receiver_email         = ( isset( $lg_google_lead_options['lg_google_lead_forms_email'] ) ) ? $lg_google_lead_options['lg_google_lead_forms_email'] : '';

	if ( isset( $request_body_array ) && ! empty( $request_body_array ) ) {
		// Validate the google key.
		if ( $google_key === $request_body_array['google_key'] || $google_key === $request_body_array['Google_key'] ) {


			handle_google_analytics_request( $request_body_array['lead_id'] );

			$user_column_data = $request_body_array['user_column_data'];
			$post_title       = '';
			$first_name       = '';
			$last_name        = '';
			$post_meta        = array();
			$post_content     = $request_body_array['lead_id'];

			if ( $user_column_data ) {
				foreach ( $user_column_data as $field ) {
					if ( 'FULL_NAME' === $field['column_id'] ) {
						$post_title = $field['string_value'];
					} elseif ( 'FIRST_NAME' === $field['column_id'] ) {
						$first_name = $field['string_value'];
					} elseif ( 'LAST_NAME' === $field['column_id'] ) {
						$last_name = $field['string_value'];
					} else {
						$post_meta[ $field['column_id'] ] = $field['string_value'];
					}
				}
			}

			if ( $first_name || $last_name ) {
				$post_title = $first_name . ' ' . $last_name;
			}

			if ( $post_title && $post_content ) {
				$query_return = wp_insert_post(
					array(
						'post_type'    => 'lg-google-lead',
						'post_title'   => $post_title,
						'post_content' => $post_content,
						'meta_input'   => $post_meta,
						'post_status'  => 'publish',
					)
				);
				if ( $query_return ) {
					// Send email to receiver.
					if ( $receiver_email ) {
						$email_subject = 'New lead received from google lead form.';
						$email_message = '<h3>New lead received with following information:</h3>';
						$email_message .= '<p>Name: ' . $post_title . '</p>';
						$email_headers = array( 'Content-Type: text/html; charset=UTF-8' );
						if ( $post_meta ) {
							foreach ( $post_meta as $field_name => $field_value ) {
								$email_message .= '<p>' . $field_name . ': ' . $field_value . '</p>';
							}
						}
						wp_mail( $receiver_email, $email_subject, $email_message, $email_headers );

					}
				} else {
					write_log( __FILE__ . ' Line' . __LINE__ . ' Error while saving form data.' );
					exit;
				}
			}
		} else {
			write_log( __FILE__ . ' Line' . __LINE__ . ' Google key verification failed' );
			exit;
		}
	} else {
		write_log( __FILE__ . ' Line' . __LINE__ . 'Invalid form data' );
		exit;
	}
}


if ( ! function_exists( 'write_log' ) ) {

	/**
	 * Log the error.
	 *
	 * @param mixed $log log error message.
	 */
	function write_log( $log ) {
		if ( true === WP_DEBUG ) {
			if ( is_array( $log ) || is_object( $log ) ) {
				error_log( print_r( $log, true ) );
			} else {
				error_log( $log );
			}
		}
	}
}

/**
 * Handle the google analytics request.
 * Send data to google analytics using Measurement Tool.
 * Reference https://developers.google.com/analytics/devguides/collection/protocol/v1/reference.
 *
 * @param string $lead_id google lead id.
 */
function handle_google_analytics_request( $lead_id ) {
	$base_url       = 'https://www.google-analytics.com/collect';
	$user_id        = $lead_id;
	$event_category = 'LG_Tracking';
	$event_action   = 'Google_Lead_Form_Submit';
	$event_label    = $lead_id;
	$tracking_id    = get_option('lg_option_google_analytics_id');
	

	//$client_id   = '0177b8a1-1f1f-4b6c-bbc7-b289b6a78df4';
	if(!$tracking_id) {
		write_log( __FILE__ . ' Line' . __LINE__ . 'Google Analytics tracking code missing.' );
		exit;
	}
	$params_array = array(
		'v'   => '1',
		't'   => 'event',
		'tid' => $tracking_id,
		//'cid' => $client_id,
		'uid' => $user_id,
		'ec'  => $event_category,
		'ea'  => $event_action,
		'el'  => $event_label,
	);
	$curl_url     = esc_url_raw( $base_url . '?' . http_build_query( $params_array ) );
	wp_remote_get( $curl_url );
}
